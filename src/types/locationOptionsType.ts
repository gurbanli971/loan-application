export type CountryOptionType = {
	countryCode: string;
	name: string;
}

export type StateOptionType = {
	stateCode: string;
	name: string;
}

export type CityOptionType = {
	name: string;
	countryCode: string;
}
