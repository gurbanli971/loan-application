export type LoanAmountType = {
	value: number;
	name: string;
};

export type LoanDurationType = {
	value: number;
	name: string;
}
