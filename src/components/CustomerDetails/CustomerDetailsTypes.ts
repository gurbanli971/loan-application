export type CustomerDataType = {
	firstname: string,
	lastname: string,
	phoneNumber: string,
	email: string,
	id: number
}
