import { describe, expect,it } from "vitest";
import { createVuetify } from "vuetify";
import * as components from "vuetify/components";
import * as directives from "vuetify/directives";

import { mount } from "@vue/test-utils";

import CustomerDetails from "./CustomerDetails.vue";

const vuetify = createVuetify({
    components,
    directives
});

describe("CustomerDetails", () => {
    it("renders the component with default data", async () => {
        const wrapper = mount(CustomerDetails, { global: { plugins: [vuetify] },
            data() {
			   return {
				   firstname: "Anna Maria Tamm",
				   lastname: "Rodriguez Espinosa",
				   phoneNumber: "+372 5289 6572",
				   email: "anna.tamm@gmail.com",
				   id: 38912052254
			   };
		   } });
        expect(wrapper.exists()).toBe(true);

        expect(wrapper.text()).toContain("Anna Maria Tamm Rodriguez Espinosa");
        expect(wrapper.text()).toContain("+372 5289 6572");
        expect(wrapper.text()).toContain("anna.tamm@gmail.com");
        expect(wrapper.text()).toContain("38912052254");

        expect(wrapper.find(".edit").exists()).toBe(true);
    });

    it("toggles edit mode when edit button is clicked", async () => {
        const wrapper = mount(CustomerDetails, { global: { plugins: [vuetify] },
			 data() {
                return {
                    firstname: "Anna Maria Tamm",
                    lastname: "Rodriguez Espinosa",
                    phoneNumber: "+372 5289 6572",
                    email: "anna.tamm@gmail.com",
                    id: 38912052254
                };
            } });


        const editButton = wrapper.find(".edit");
        await editButton.trigger("click");

        expect(wrapper.text()).toContain("Change your contact Info");
    });
});
