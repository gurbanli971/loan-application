import { describe, expect,test } from "vitest";

import { mount } from "@vue/test-utils";

import Logo from "./Logo.vue";

describe("Logo", () => {
    test("renders component correctly", () => {
        const wrapper = mount(Logo);

        expect(wrapper.find("img").attributes("alt")).toBe("inbank-logo");
    });
});
