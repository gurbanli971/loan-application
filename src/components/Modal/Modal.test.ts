import { describe, expect,test } from "vitest";

import { mount } from "@vue/test-utils";

import Modal from "./Modal.vue";

describe("Modal", () => {
    test("renders modal with correct title", () => {
        const wrapper = mount(Modal, {
            props: {
                title: "Modal Title"
            }
        });

        expect(wrapper.find(".modal-inner h3").text()).toBe("Modal Title");
    });

    test("emits close event when on button click", () => {
        const wrapper = mount(Modal, {
            props: {
                title: "Modal Title"
            }
        });

        const closeButton = wrapper.find("button");

        closeButton.trigger("click");

        expect(wrapper.emitted("close")).toBeTruthy();
    });
});
