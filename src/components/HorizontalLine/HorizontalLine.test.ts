import { describe, expect,test } from "vitest";
import { createVuetify } from "vuetify";
import * as components from "vuetify/components";
import * as directives from "vuetify/directives";

import { mount } from "@vue/test-utils";

import HorizontalLine from "./HorizontalLine.vue";

const vuetify = createVuetify({
    components,
    directives
});

describe("HorizontalLine", () => {
    test("renders component correctly", () => {
        const wrapper = mount(HorizontalLine, { global: { plugins: [vuetify] } });
        expect(wrapper.classes()).toContain("horizontal");
    });
});
