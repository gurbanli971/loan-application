import { describe, expect,test } from "vitest";
import { createVuetify } from "vuetify";
import * as components from "vuetify/components";
import * as directives from "vuetify/directives";

import { mount } from "@vue/test-utils";

import LoanDetails from "./LoanDetails.vue";

const vuetify = createVuetify({
    components,
    directives
});

describe("LoanDetails", () => {
    test("renders component correctly", () => {
        const wrapper = mount(LoanDetails,{ global: { plugins: [vuetify] } });

        expect(wrapper.find("img").attributes("alt")).toBe("arrow-right");

        expect(wrapper.find("span").text()).toBe("200 €");
    });
});
