import { CityOptionType, CountryOptionType, StateOptionType } from "@/types/locationOptionsType";

export type PersonalDataType = {
	selectedState: StateOptionType | null,
    selectedCity: CityOptionType | null,
    selectedVillage: CityOptionType | null,
    streetName: string,
    houseName: string,
    apartmentName: string,
    postalCode: string,
    firstName: string,
    lastName: string,
    personalId: string,
    residency: CountryOptionType | null,
    birthPlace: string,
    birthDate: string
}
