import { LoanAmountType, LoanDurationType } from "@/types/loanOptionsType";

export const getLoanAmountOptions = (): LoanAmountType[] => {
    const options: LoanAmountType[] = [];

    for (let i = 200; i <= 1000; i += 100) {
        options.push({
            value: i,
            name: `${i} €`
        });
    }

    return options;
};

export const getLoanDurationOptions = (): LoanDurationType[] => {
    const months = [2, 4, 6, 12, 16, 18, 20, 24, 28, 30, 32, 36];

    const options = months.map((month) => {
        return {
            value: month,
            name: `${month} months`
        };
    });

    return options;
};
