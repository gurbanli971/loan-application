import csc, { ICity, IState } from "country-state-city";

import { CityOptionType, CountryOptionType, StateOptionType } from "@/types/locationOptionsType";

export const getCountryOptions = (): CountryOptionType[] => {
    const countries = csc.getAllCountries();

    return countries.map((country) => {
        return {
            name: country.name,
            countryCode: country.isoCode
        };});
};

export const getStateOptions = (countryCode: string): StateOptionType[] => {
    const states  = csc.getStatesOfCountry(countryCode);

    return states.map((state: IState) => { return {
        name: state.name,
        stateCode: state.isoCode
    };});
};

export const getCityOptions = (countryCode: string, stateCode: string): CityOptionType[] => {
    const cities  = csc.getCitiesOfState(countryCode, stateCode);

    return cities.map((city: ICity) => { return {
        name: city.name,
        countryCode: city.countryCode
    };});
};
