import { describe, expect,test } from "vitest";

import { getLoanAmountOptions, getLoanDurationOptions } from "@/helpers/loanOptions";

describe("Loan Options", () => {
    test("returns an array of loan amount options", () => {
        const loanOptions = getLoanAmountOptions();

        expect(Array.isArray(loanOptions)).toBe(true);
        expect(loanOptions).toHaveLength(9);

        loanOptions.forEach((option) => {
            expect(option).toHaveProperty("value");
            expect(option).toHaveProperty("name");

            expect(typeof option.value).toBe("number");
            expect(typeof option.name).toBe("string");
        });
    });

    test("returns an array of loan period options", () => {
        const options = getLoanDurationOptions();

        expect(Array.isArray(options)).toBe(true);
        expect(options).toHaveLength(12);

        options.forEach((option) => {
            expect(option).toHaveProperty("value");
            expect(option).toHaveProperty("name");

            expect(typeof option.value).toBe("number");
            expect(typeof option.name).toBe("string");
        });
    });
});
