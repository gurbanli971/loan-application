import { describe, expect,test } from "vitest";

import { isValidEmail, isValidPhoneNumber } from "@/helpers/formValidations";

describe("Form Validations", () => {
    test("Email is valid", () => {
        expect(isValidEmail("test@test.com")).toBe(true);
    });

    test("Email is invalid", () => {
        expect(isValidEmail("test@.com")).toBe(false);
    });

    test("Phone number is valid", () => {
        expect(isValidPhoneNumber("+37265738263")).toBe(true);
    });

    test("Phone number is invalid", () => {
        expect(isValidPhoneNumber("12345678")).toBe(false);
    });
});
