# Loan Application

## Technologies used
- Vite create(typescript template)
- Vue(composition Api)
- Styling (SCSS)
- Tests (Vitest)
- Lint (ESLint)
- npm country-state-city package for select options data

### Install
```
  npm install
```

### Dev mode
```
  npm run dev
```

### Build and serve bundle
```
  npm run build
  npx serve -s dist
```

### Tests
```
  npm run test:unit
```

### EsLint
```
  npm run lint
  npm run lint:fix
```